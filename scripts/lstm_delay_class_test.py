# Federico Vendramin 20/12/17
#
# Test script: LSTM DISCRETE output delayed input
#

import random
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, LSTM, Masking, Dropout, Input, TimeDistributed, GRU

environment = []
output = []

in_list = []
out_list = []

test_in_list = []
test_out_list = []

nr_training_examples = int(input("Insert number of training examples: "))
maxlen = 7
delay = 3
ratio = 0.3

for i in range(nr_training_examples):
    # Resets
    environment.clear()
    output.clear()

    num = round(random.uniform(0,4))
    signal = round(random.uniform(1,6))
    print("Selected Num: ", num)
    print("Selected Timestep: ", signal)

    for j in range(0,maxlen):
        if (j == 0):
            if (num == 0):
                environment.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])
            if (num == 1):
                environment.append([0.0, 1.0, 0.0, 0.0, 0.0, 0.0])
            if (num == 2):
                environment.append([0.0, 0.0, 1.0, 0.0, 0.0, 0.0])
            if (num == 3):
                environment.append([0.0, 0.0, 0.0, 1.0, 0.0, 0.0])
            if (num == 4):
                environment.append([0.0, 0.0, 0.0, 0.0, 1.0, 0.0])
        elif (j == signal):
            environment.append([0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
        else:
            environment.append([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

        if (j == signal):
            if (num == 0):
                output.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])
            if (num == 1):
                output.append([0.0, 1.0, 0.0, 0.0, 0.0, 0.0])
            if (num == 2):
                output.append([0.0, 0.0, 1.0, 0.0, 0.0, 0.0])
            if (num == 3):
                output.append([0.0, 0.0, 0.0, 1.0, 0.0, 0.0])
            if (num == 4):
                output.append([0.0, 0.0, 0.0, 0.0, 1.0, 0.0])
        else:
            output.append([0.0, 0.0, 0.0, 0.0, 0.0, 1.0])

    print("Env: "+str(i)+ " "+str(environment))
    print("Out: "+str(i)+ " "+str(output))

    if (i < ratio * nr_training_examples):
        test_in_list.append(list(environment))
        test_out_list.append(list(output))
    else:
        in_list.append(list(environment))
        out_list.append(list(output))

x_train = np.array(list(in_list))
y_train = np.array(list(out_list))
x_test = np.array(list(test_in_list))
y_test = np.array(list(test_out_list))

print("x_train shape: ", x_train.shape)
print("y_train shape: ", y_train.shape)

print("x_test shape: ", x_test.shape)
print("y_test shape: ", y_test.shape)

model = Sequential()
nr_units = 50
#model.reset_states()
#model.add(GRU(units=nr_units, return_sequences=True, input_shape=(None, 1)))
model.add(LSTM(units=nr_units, return_sequences=True, input_shape=(None, 6)))
#model.add(LSTM(units=nr_units, return_sequences=True))

#model.add(Dropout(0.2))
model.add(TimeDistributed(Dense(6, activation='softmax')))
#opt = SGD(lr=0.01)
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model.summary())
nr_epochs = 5
nr_batch = 4
model.fit(x_train, y_train, epochs=nr_epochs, batch_size=nr_batch)


# Final evaluation of the model
scores = model.evaluate(x_test, y_test, verbose=0)
#print("Score: Accuracy=", scores[1])

#-----------EVAL TEST----------->
new_model = Sequential()
#new_model.add(GRU(units=nr_units, return_sequences=True, stateful=True, batch_input_shape=(1,1,1)))
new_model.add(LSTM(units=nr_units, return_sequences=True, stateful=True, batch_input_shape=(1,1,6)))
#new_model.add(LSTM(units=nr_units, return_sequences=True))

#new_model.add(Dropout(0.2))
new_model.add(TimeDistributed(Dense(6, activation='softmax')))
new_model.compile(loss='mse', optimizer='adam')
new_model.set_weights(model.get_weights())
#------------------------------->

while(True):
    num = int(input("Insert 0 - 4 Input Number: "))
    signal = int(input("Insert 1 - 7 Trigger Timestamp: "))

    runtime_list = []
    new_model.reset_states()

    for i in range(maxlen+1):
        environment.clear()
        if (i == 0):
            if (num == 0):
                environment.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])
            if (num == 1):
                environment.append([0.0, 1.0, 0.0, 0.0, 0.0, 0.0])
            if (num == 2):
                environment.append([0.0, 0.0, 1.0, 0.0, 0.0, 0.0])
            if (num == 3):
                environment.append([0.0, 0.0, 0.0, 1.0, 0.0, 0.0])
            if (num == 4):
                environment.append([0.0, 0.0, 0.0, 0.0, 1.0, 0.0])
        elif(i == signal):
            environment.append([0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
        else:
            environment.append([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

        runtime_list.clear()
        runtime_list.append(list(environment))
        x_runtime = np.array(list(runtime_list))
        #print("X_runtime: ", x_runtime)
        pred = new_model.predict(x_runtime)

        print("prediction: " + str(round(pred[0][0][0], 2)) + " "+str(round(pred[0][0][1], 2)) \
                        +" "+str(round(pred[0][0][2], 2)) +" "+ str(round(pred[0][0][3], 2)) \
                        +" "+ str(round(pred[0][0][4], 2))+" "+ str(round(pred[0][0][5], 2)))