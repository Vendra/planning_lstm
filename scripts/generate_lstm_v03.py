# Federico Vendramin 21-12-17
#
# Generate an artificial dataset and then train a LSTM network
# Discretized Arguments space, so that their generation is now a classification problem

import random
import numpy as np
from keras.models import Model
from keras.utils import plot_model
from keras.layers import Dense, LSTM, Masking, Dropout, Input, TimeDistributed
import matplotlib.pyplot as plt
from swap_dataset_generator import SwapDatasetGenerator

#For the sake of readability
np.set_printoptions(formatter={'float': lambda x: "{0:0.1f}".format(x)})

nr_training_examples = int(input("Insert number of training example: "))
error_rate = float(input("Insert failure probability [0-1]: "))
ratio = 0.2
training_example = []  # Contains all training examples
maxlen = 30
nr_units = 50
nr_epochs = 150
nr_batch = 4

generator = SwapDatasetGenerator(error_rate, nr_training_examples, maxlen, pad=True, ratio=ratio)
#(x_train, y_train, arg1_train, arg2_train, x_test, y_test, arg1_test, arg2_test) = generator.generate_discrete_mod()
(x_train, y_train, arg1_train, arg2_train, x_test, y_test, arg1_test, arg2_test) = generator.generate_discrete_single(ratio=0.05)
#(x_train, y_train, arg1_train, arg2_train, x_test, y_test, arg1_test, arg2_test) = generator.generate_discrete_complete(ratio=0.05)


#print(x_train.shape)
#for k in range(0,x_train.shape[1]):
#    print("Timestep: ", k)
#    print("1_X  : ", x_train[0][k][0:10])
#    print("1_Y  : ", x_train[0][k][10:20])
#    print("2_X  : ", x_train[0][k][20:30])
#    print("2_Y  : ", x_train[0][k][30:40])
#    print("T_X  : ", x_train[0][k][40:50])
#    print("T_Y  : ", x_train[0][k][50:60])
#    print("Grip : ", x_train[0][k][60:])
#    print("Act  : ", y_train[0][k])
#    print("Arg1 : ", arg1_train[0][k][0:10])
#    print("Arg2 : ", arg2_train[0][k][0:10])
#    print()

main_input = Input(shape=(None, 61), dtype='float32')
masked_input = Masking(mask_value= 0.0)(main_input)

lstm1 = LSTM(units=nr_units, return_sequences=True)(masked_input)
lstm2 = LSTM(units=nr_units, return_sequences=True)(lstm1)
lstm_out = LSTM(units=nr_units, return_sequences=True)(lstm2)
#lstm_dropped = Dropout(0.2)(lstm_out)

out = TimeDistributed(Dense(4,   activation='softmax'))(lstm_out)
arg1 = TimeDistributed(Dense(10, activation='softmax'))(lstm_out)
arg2 = TimeDistributed(Dense(10, activation='softmax'))(lstm_out)

model = Model(inputs=[main_input], outputs=[out, arg1, arg2])
model.compile(loss=["categorical_crossentropy","categorical_crossentropy", "categorical_crossentropy"], \
              optimizer='adam', metrics=['accuracy'])

print(model.summary())
#plot_model(model, to_file='discrete_3stacked_model.png')

model.reset_states()
history = model.fit(x_train, [y_train, arg1_train, arg2_train], validation_split=0.25,
                    epochs=nr_epochs, batch_size=nr_batch, verbose=2)

#Display Data
print(history.history.keys())
fig1 = plt.figure(1)
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Total Loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['train', 'val'], loc='upper left')

fig2 = plt.figure(2)
plt.plot(history.history['time_distributed_1_loss'])
plt.plot(history.history['val_time_distributed_1_loss'])
plt.title('1 Output Layer Loss - Action')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['train', 'val'], loc='upper left')

fig3 = plt.figure(3)
plt.plot(history.history['time_distributed_2_loss'])
plt.plot(history.history['val_time_distributed_2_loss'])
plt.title('2 Output Layer Loss - Args 1')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['train', 'val'], loc='upper left')

fig4 = plt.figure(4)
plt.plot(history.history['time_distributed_3_loss'])
plt.plot(history.history['val_time_distributed_3_loss'])
plt.title('3 Output Layer Loss - Args 2')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['train', 'val'], loc='upper left')

fig5 = plt.figure(5)
plt.plot(history.history['time_distributed_1_acc'])
plt.plot(history.history['val_time_distributed_1_acc'])
plt.title('Action Accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['train', 'val'], loc='upper left')

fig6 = plt.figure(6)
plt.plot(history.history['time_distributed_2_acc'])
plt.plot(history.history['val_time_distributed_2_acc'])
plt.title('Single Argument Accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['train', 'val'], loc='upper left')

plt.show()
#fig1.savefig('./SameTestImg/test4_total_loss.png')
#fig2.savefig('./SameTestImg/test4_action_loss.png')
#fig3.savefig('./SameTestImg/test4_arg1_loss.png')
#fig4.savefig('./SameTestImg/test4_arg2_loss.png')
#fig5.savefig('./SameTestImg/test4_action_accuracy.png')
#fig6.savefig('./SameTestImg/test4_arg_accuracy.png')
#fig1.clf()
#fig2.clf()
#fig3.clf()
#fig4.clf()
#fig5.clf()
#fig6.clf()




# ----------- FINAL TEST MODEL ---------->
scores = model.evaluate(x_test, [y_test, arg1_test, arg2_test], verbose=2)
print(model.metrics_names)
print(scores)
print("Action Accuracy: %.2f%%" % (scores[4] * 100))
print("Args 1 Accuracy: %.2f%%" % (scores[5] * 100))
print("Args 2 Accuracy: %.2f%%" % (scores[6] * 100))

# -----------NEW TEST LONG SEQ + ERR ---------->
print("Long Sequence Test - generalization")
long_generator = SwapDatasetGenerator(err=0.45, nr_examples=500, maxlen=100, pad=True, ratio=0.0)
(x_long, y_long, arg1_long, arg2_long, empty1, empty2, empty3, empty4) = long_generator.generate_discrete_mod()
#(x_long, y_long, arg1_long, arg2_long, empty1, empty2, empty3, empty4) = long_generator.generate_discrete_single(0.05)
#(x_long, y_long, arg1_long, arg2_long, empty1, empty2, empty3, empty4) = long_generator.generate_discrete_complete(0.05)
long_scores = model.evaluate(x_long, [y_long, arg1_long, arg2_long], verbose=2)

print(model.metrics_names)
print(long_scores)
print("Action Accuracy: %.2f%%" % (long_scores[4] * 100))
print("Args 1 Accuracy: %.2f%%" % (long_scores[5] * 100))
print("Args 2 Accuracy: %.2f%%" % (long_scores[6] * 100))



# ------------------------------------------------------------>



save = input("Save model?y/N")
if (save == 'y'):
    model.save("steps_"+str(nr_training_examples)+"_units_"+str(nr_units)+"_epochs_"+str(nr_epochs)+"_batch_"+str(nr_batch))
    print("Model saved")

# -----------CREATE NEW MODEL STATEFUL LOAD WEIGHTS ---------->

eval_input = Input(batch_shape=(1, 1, 61), dtype='float32')
eval_masked_input = Masking(mask_value= 0.0)(eval_input)

eval_lstm1 = LSTM(units=nr_units, return_sequences=True, stateful=True)(eval_masked_input)
eval_lstm2 = LSTM(units=nr_units, return_sequences=True, stateful=True)(eval_lstm1)
eval_lstm_out = LSTM(units=nr_units, return_sequences=True, stateful=True)(eval_lstm2)
#eval_lstm_dropped = Dropout(0.2)(eval_lstm_out)

eval_out = TimeDistributed(Dense(4,   activation='softmax'))(eval_lstm_out)
eval_arg1 = TimeDistributed(Dense(10, activation='softmax'))(eval_lstm_out)
eval_arg2 = TimeDistributed(Dense(10, activation='softmax'))(eval_lstm_out)

eval_model = Model(inputs=[eval_input], outputs=[eval_out, eval_arg1, eval_arg2])
eval_model.set_weights(model.get_weights())


# Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
x1 = round(random.uniform(1, 9))
y1 = round(random.uniform(1, 9))

x2 = round(random.uniform(1, 9))
y2 = round(random.uniform(1, 9))

x_temp = round(random.uniform(1, 9))
y_temp = round(random.uniform(1, 9))

while (x1 == x2):
    x2 = round(random.uniform(1, 9))
while (y1 == y2):
    y2 = round(random.uniform(1, 9))

gripper_occupied = 0.0  # Default gripper empty at beginning
# Give environment to lstm, see prediction
env = []
test_list = []
env.append(generator.generate_environment_mod(x1, y1, x2, y2, gripper_occupied, x_temp, y_temp))

test_list.append(list(env))

x_test = np.array(list(test_list))
print("Env  : ")
print("1_X  : ", x_test[0][0][0:10])
print("1_Y  : ", x_test[0][0][10:20])
print("2_X  : ", x_test[0][0][20:30])
print("2_Y  : ", x_test[0][0][30:40])
print("T_X  : ", x_test[0][0][40:50])
print("T_Y  : ", x_test[0][0][50:60])
print("Grip : ", x_test[0][0][60:])

act_pred, arg1_pred, arg2_pred = eval_model.predict(x_test, verbose=0)
#print("ACT Pred Shape: ", act_pred.shape)
#print("ARG Pred Shape: ", arg_pred.shape)
print("Act      : ", act_pred[0][0])
print("PlaceX   : ", arg1_pred[0][0][0:10])
print("PlaceY   : ", arg2_pred[0][0][0:10])


user_env = []
user_in = []
env_list = []

while(act_pred[0][0][3] < 0.5):
    print("Insert Environment: ")

    user_in.clear()
    for i in range(5):
        n = input("Env: ")
        user_in.append(float(n))

    env.clear()
    env.append(generator.generate_environment_mod(user_in[0], user_in[1], user_in[2], user_in[3], user_in[4]))

    print("Feeding environment to model.")
    print(" ", env[0][0:10])
    print(" ", env[0][10:20])
    print(" ", env[0][20:30])
    print(" ", env[0][30:40])
    print(" ", env[0][40:50])
    print(" ", env[0][50:60])
    print(" ", env[0][60:])

    env_list.clear()
    env_list.append(list(env))
    user_env = np.array(list(env_list))

    act_pred, arg1_pred, arg2_pred = eval_model.predict(user_env, verbose=0)

    print("Act: ", act_pred[0][0])
    print("Arg: ", arg1_pred[0][0][0:10])
    print("   : ", arg2_pred[0][0][0:10])

print("Terminated. Value: " + str(act_pred[0][0][3]))
