# Federico Vendramin 19/12/17
#
# Echo Test: Learn Fixed Input Output Delay
# Repeated Input + First timestep Input


import random
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, LSTM, TimeDistributed
import matplotlib.pyplot as plt

environment = []
output = []

in_list = []
out_list = []

test_in_list = []
test_out_list = []

#Ask for input values
nr_training_examples = int(input("Insert number of training examples: "))
delay = int(input("Insert timesteps delay: "))
print("Insert \"0\" for REPEATING the input number")
signal = int(input("Insert \"1\" for SINGLE input number:"))
singleInput = False
if signal > 0:
    singleInput = True

maxlen = 7
ratio = 0.2

for i in range(nr_training_examples):
    # Resets
    environment.clear()
    output.clear()

    num = round(random.uniform(0,1), 3)
    for j in range(0,maxlen):
        if (singleInput and j != 0):
            environment.append([0])
        else:
            environment.append([num])

        if (j == delay):
            output.append([num])
        else:
            output.append([0])
    #print("Env: "+str(i)+ " "+str(environment))
    #print("Out: "+str(i)+ " "+str(output))

    if (i < ratio * nr_training_examples):
        test_in_list.append(list(environment))
        test_out_list.append(list(output))
    else:
        in_list.append(list(environment))
        out_list.append(list(output))

x_train = np.array(list(in_list))
y_train = np.array(list(out_list))
x_test = np.array(list(test_in_list))
y_test = np.array(list(test_out_list))

print("x_train shape: ", x_train.shape)
print("y_train shape: ", y_train.shape)

print("x_test shape: ", x_test.shape)
print("y_test shape: ", y_test.shape)

model = Sequential()
nr_units = 25
#model.reset_states()
#model.add(GRU(units=nr_units, return_sequences=True, input_shape=(None, 1)))
model.add(LSTM(units=nr_units, return_sequences=True, input_shape=(maxlen, 1)))
#model.add(LSTM(units=nr_units, return_sequences=True))

#model.add(Dropout(0.2))
model.add(TimeDistributed(Dense(1)))
#opt = SGD(lr=0.01)
model.compile(loss='mse', optimizer='adam')
print(model.summary())
nr_epochs = 15
nr_batch = 4
history = model.fit(x_train, y_train, epochs=nr_epochs, batch_size=nr_batch,
                    validation_split=0.25, verbose=2)

#Display Data
print(history.history.keys())
plt.figure(1)
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Total Loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['train', 'val'], loc='upper left')

plt.show()

# Final evaluation of the model
scores = model.evaluate(x_test, y_test, verbose=0)
print(model.metrics_names)
print(scores)

#-----------EVAL TEST----------->
new_model = Sequential()
#new_model.add(GRU(units=nr_units, return_sequences=True, stateful=True, batch_input_shape=(1,1,1)))
new_model.add(LSTM(units=nr_units, return_sequences=True, stateful=True, batch_input_shape=(1,1,1)))
#new_model.add(LSTM(units=nr_units, return_sequences=True))

#new_model.add(Dropout(0.2))
new_model.add(TimeDistributed(Dense(1)))
new_model.compile(loss='mse', optimizer='adam')
new_model.set_weights(model.get_weights())

#------------------------------->

number = float(input("Insert 0.0 - 1.0 number: "))
while(number > 0):
    runtime_list = []
    new_model.reset_states()

    for i in range(maxlen):
        environment.clear()
        if (singleInput and i != 0):
            environment.append([0.0])
        else:
            environment.append([number])
        runtime_list.clear()
        runtime_list.append(list(environment))
        x_runtime = np.array(list(runtime_list))
        print("Input: ", x_runtime)
        pred = new_model.predict(x_runtime)
        print("Prediction: ", pred)
        print()

    number = float(input("Insert 0.0 - 1.0 number: "))
