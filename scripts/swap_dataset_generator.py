# Federico Vendramin 14/12/17
#
# Artificial Dataset Generator
#
# Generates the training examples for the swapping problem
# Each training example is composed of two sequences:
# Input/Environment: < X1, Y1, X2, Y2, Gripper_Status >
# Output/Prediction: < Pick(Obj1), Pick(Obj2), Place(), X, Y, END >
# There is a probability that an action fails

__author__ = 'Federico Vendramin'

import random, math
import numpy as np

class SwapDatasetGenerator:

    def __init__(self, err, nr_examples, maxlen=25, pad=True, ratio=.0):
        self.error_rate = err
        self.nr_training_examples = nr_examples
        self.maxlen = maxlen
        self.pad = pad
        self.ratio = ratio

    #Classic generation: continuous domain - 1 Output Layer
    def generate(self):
        environment = []
        output = []

        in_list = []
        out_list = []

        test_in_list = []
        test_out_list = []

        for i in range(self.nr_training_examples):
            # Resets
            environment.clear()
            output.clear()
            # Note: Position (0,0) should be reserved for when the cube is gripped

            # Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2)  [0,1]
            x1 = random.uniform(0, 1)
            y1 = random.uniform(0, 1)

            x2 = random.uniform(0, 1)
            y2 = random.uniform(0, 1)

            while (x1 == x2):
                x2 = random.uniform(0, 1)
            while (y1 == y2):
                y2 = random.uniform(0, 1)

            gripper_occupied = 0.0  # Default gripper empty at beginning

            environment.append([x1, y1, x2, y2, gripper_occupied])

            # Generate Output: Pick(object1)
            output.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])

            # Update Environment: (introduce certain % that pick fails)
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([random.uniform(0, 1), random.uniform(0, 1),
                                    x2, y2, 0.0])
                output.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])

            # Now Pick is okay, can make the place, set occupied gripper
            environment.append([0.0, 0.0, x2, y2, 1.0])
            temp_x = random.uniform(0, 1)
            temp_y = random.uniform(0, 1)
            # Generate Output: place on random temp position
            output.append([0.0, 0.0, 1.0, temp_x, temp_y, 0.0])

            # Update Environment: (introduce certain % that the place fails, wrong position)
            while (random.uniform(0, 1) <= self.error_rate):
                # Place the cube in random position
                new_x = -1.0
                new_y = -1.0
                generate = True
                while (abs(new_x - temp_x) < 0.05 or abs(new_y - temp_y) < 0.05 or generate):
                    new_x = random.uniform(0, 1)
                    new_y = random.uniform(0, 1)
                    generate = False

                environment.append([new_x, new_y, x2, y2, 0.0])
                # re-execute pick OBJ1
                output.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append([random.uniform(0, 1), random.uniform(0, 1), x2, y2, 0.0])
                    output.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])

                # Pick okay
                environment.append([0.0, 0.0, x2, y2, 1.0])
                # Place again
                output.append([0.0, 0.0, 1.0, temp_x, temp_y, 0.0])

            # Successful, environment observation is correct now
            environment.append([temp_x, temp_y, x2, y2, 0.0])
            # Can proceed with output: pick OBJ2
            output.append([0.0, 1.0, 0.0, 0.0, 0.0, 0.0])

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([temp_x, temp_y, random.uniform(0, 1), random.uniform(0, 1), 0.0])
                output.append([0.0, 1.0, 0.0, 0.0, 0.0, 0.0])

            # Pick successful, generate correct environment observation, set occupied gripper
            environment.append([temp_x, temp_y, 0.0, 0.0, 1.0])
            # Place Object grasped (OBJ2) in original position of OBJ1
            output.append([0.0, 0.0, 1.0, x1, y1, 0.0])

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([temp_x, temp_y, random.uniform(0, 1), random.uniform(0, 1), 0.0])
                # Re-execute pick OBJ2
                output.append([0.0, 1.0, 0.0, 0.0, 0.0, 0.0])

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append([temp_x, temp_y, random.uniform(0, 1), random.uniform(0, 1), 0.0])
                    output.append([0.0, 1.0, 0.0, 0.0, 0.0, 0.0])

                # Pick Okay
                environment.append([temp_x, temp_y, 0.0, 0.0, 1.0])
                # Retry Place OBJ2
                output.append([0.0, 0.0, 1.0, x1, y1, 0.0])

            # Succeded Place, generate env
            environment.append([temp_x, temp_y, x1, y1, 0.0])
            # Output: Pick OBJ1
            output.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([random.uniform(0, 1), random.uniform(0, 1), x1, y1, 0.0])
                output.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])

            # Pick succeded
            environment.append([0.0, 0.0, x1, y1, 1.0])
            # Place OBJ1 in OBJ2 orinal position
            output.append([0.0, 0.0, 1.0, x2, y2, 0.0])

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([random.uniform(0, 1), random.uniform(0, 1), x1, y1, 0.0])
                output.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])

                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append([random.uniform(0, 1), random.uniform(0, 1), x1, y1, 0.0])
                    output.append([1.0, 0.0, 0.0, 0.0, 0.0, 0.0])

                environment.append([0.0, 0.0, x1, y1, 1.0])
                output.append([0.0, 0.0, 1.0, x2, y2, 0.0])

            # Place successful
            environment.append([x2, y2, x1, y1, 0.0])
            output.append([0.0, 0.0, 0.0, 0.0, 0.0, 1.0])

            # Manual Padding
            if (len(environment) > self.maxlen):
                print("ERROR SEQ TOO LONG")

            if (self.pad == True):
                while (len(environment) < self.maxlen):
                    environment.append([-1.0, -1.0, -1.0, -1.0, -1.0])
                    output.append([-1.0, -1.0, -1.0, -1.0, -1.0, -1.0])


            if (i < self.ratio * self.nr_training_examples):
                test_in_list.append(list(environment))
                test_out_list.append(list(output))
            else:
                in_list.append(list(environment))
                out_list.append(list(output))

        return (np.array(list(in_list)), np.array(list(out_list)), \
               np.array(list(test_in_list)), np.array(list(test_out_list)))

    #Modified generation: continuous domain - 2 Output Layers
    def generate_mod(self):
        # Sequence of environment observation and output for the training example
        environment = []
        output = []
        out_arg = []

        in_list = []
        out_list = []
        arg_list = []

        test_in_list = []
        test_out_list = []
        test_arg_list = []

        # Dataset Generation
        for i in range(self.nr_training_examples):
            # Resets
            environment.clear()
            output.clear()
            out_arg.clear()
            # Note: Position (0,0) should be reserved for when the cube is gripped

            # Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
            x1 = random.uniform(0, 1)
            y1 = random.uniform(0, 1)

            x2 = random.uniform(0, 1)
            y2 = random.uniform(0, 1)

            while (x1 == x2):
                x2 = random.uniform(0, 1)
            while (y1 == y2):
                y2 = random.uniform(0, 1)

            gripper_occupied = 0.0  # Default gripper empty at beginning

            environment.append([x1, y1, x2, y2, gripper_occupied])
            # Generate Output: Pick(object1)
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg.append([-1.0, -1.0])

            # Update Environment: (introduce certain % that pick fails)
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([random.uniform(0, 1), random.uniform(0, 1),
                                    x2, y2, 0.0])
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

            # Now Pick is okay, can make the place, set occupied gripper
            environment.append([0.0, 0.0, x2, y2, 1.0])
            temp_x = random.uniform(0, 1)
            temp_y = random.uniform(0, 1)
            # Generate Output: place on random temp position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg.append([temp_x, temp_y])

            # Update Environment: (introduce certain % that the place fails, wrong position)
            while (random.uniform(0, 1) <= self.error_rate):
                # Place the cube in random position
                new_x = -1.0
                new_y = -1.0
                generate = True
                while (abs(new_x - temp_x) < 0.05 or abs(new_y - temp_y) < 0.05 or generate):
                    new_x = random.uniform(0, 1)
                    new_y = random.uniform(0, 1)
                    generate = False

                environment.append([new_x, new_y, x2, y2, 0.0])
                # re-execute pick OBJ1
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append([random.uniform(0, 1), random.uniform(0, 1), x2, y2, 0.0])
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg.append([-1.0, -1.0])
                # Pick okay
                environment.append([0.0, 0.0, x2, y2, 1.0])
                # Place again
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg.append([temp_x, temp_y])

            # Successful, environment observation is correct now
            environment.append([temp_x, temp_y, x2, y2, 0.0])
            # Can proceed with output: pick OBJ2
            output.append([0.0, 1.0, 0.0, 0.0])
            out_arg.append([-1.0, -1.0])

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([temp_x, temp_y, random.uniform(0, 1), random.uniform(0, 1), 0.0])
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

            # Pick successful, generate correct environment observation, set occupied gripper
            environment.append([temp_x, temp_y, 0.0, 0.0, 1.0])
            # Place Object grasped (OBJ2) in original position of OBJ1
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg.append([x1, y1])

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([temp_x, temp_y, random.uniform(0, 1), random.uniform(0, 1), 0.0])
                # Re-execute pick OBJ2
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append([temp_x, temp_y, random.uniform(0, 1), random.uniform(0, 1), 0.0])
                    output.append([0.0, 1.0, 0.0, 0.0])
                    out_arg.append([-1.0, -1.0])

                # Pick Okay
                environment.append([temp_x, temp_y, 0.0, 0.0, 1.0])
                # Retry Place OBJ2
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg.append([x1, y1])

            # Succeded Place, generate env
            environment.append([temp_x, temp_y, x1, y1, 0.0])
            # Output: Pick OBJ1
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg.append([-1.0, -1.0])

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([random.uniform(0, 1), random.uniform(0, 1), x1, y1, 0.0])
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

            # Pick succeded
            environment.append([0.0, 0.0, x1, y1, 1.0])
            # Place OBJ1 in OBJ2 orinal position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg.append([x2, y2])

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([random.uniform(0, 1), random.uniform(0, 1), x1, y1, 0.0])
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append([random.uniform(0, 1), random.uniform(0, 1), x1, y1, 0.0])
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg.append([-1.0, -1.0])

                environment.append([0.0, 0.0, x1, y1, 1.0])
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg.append([x2, y2])

            # Place successful
            environment.append([x2, y2, x1, y1, 0.0])
            output.append([0.0, 0.0, 0.0, 1.0])
            out_arg.append([-1.0, -1.0])

            # Manual Padding
            if (len(environment) > self.maxlen):
                print("ERROR SEQ TOO LONG")

            if( self.pad == True):
                while (len(environment) < self.maxlen):
                    environment.append([-1.0, -1.0, -1.0, -1.0, -1.0])
                    output.append([-1.0, -1.0, -1.0, -1.0])
                    out_arg.append([-1.0, -1.0])

            if (i < self.ratio * self.nr_training_examples):
                test_in_list.append(list(environment))
                test_out_list.append(list(output))
                test_arg_list.append(list(out_arg))
            else:
                in_list.append(list(environment))
                out_list.append(list(output))
                arg_list.append(list(out_arg))

        return (np.array(list(in_list)), np.array(list(out_list)), np.array(list(arg_list)), \
                np.array(list(test_in_list)), np.array(list(test_out_list)), np.array(list(test_arg_list)))

    #Modified generation:  continuous domain - 2 Output Layers
    #No Random Position Error - Externally Generated
    def generate_mod_ext_temp(self):
        # Sequence of environment observation and output for the training example
        environment = []
        output = []
        out_arg = []

        in_list = []
        out_list = []
        arg_list = []

        test_in_list = []
        test_out_list = []
        test_arg_list = []

        # Dataset Generation
        for i in range(self.nr_training_examples):
            # Resets
            environment.clear()
            output.clear()
            out_arg.clear()
            # Note: Position (0,0) should be reserved for when the cube is gripped

            # Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
            x1 = random.uniform(0, 1)
            y1 = random.uniform(0, 1)

            x2 = random.uniform(0, 1)
            y2 = random.uniform(0, 1)

            #Generates temp (X,Y) as if was provided as input
            temp_x = random.uniform(0, 1)
            temp_y = random.uniform(0, 1)

            while (x1 == x2):
                x2 = random.uniform(0, 1)
            while (y1 == y2):
                y2 = random.uniform(0, 1)

            gripper_occupied = 0.0  # Default gripper empty at beginning

            environment.append([x1, y1, x2, y2, temp_x, temp_y, gripper_occupied])
            # Generate Output: Pick(object1)
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg.append([-1.0, -1.0])

            # Update Environment: (introduce certain % that pick fails)
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([random.uniform(0, 1), random.uniform(0, 1),
                                    x2, y2, 0.0, 0.0, 0.0])
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

            # Now Pick is okay, can make the place, set occupied gripper
            environment.append([0.0, 0.0, x2, y2, 0.0, 0.0, 1.0])

            # Generate Output: place on random temp position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg.append([temp_x, temp_y])

            # Update Environment: (introduce certain % that the place fails, wrong position)
            while (random.uniform(0, 1) <= self.error_rate):
                # Place the cube in random position
                new_x = -1.0
                new_y = -1.0
                generate = True
                while (abs(new_x - temp_x) < 0.05 or abs(new_y - temp_y) < 0.05 or generate):
                    new_x = random.uniform(0, 1)
                    new_y = random.uniform(0, 1)
                    generate = False

                environment.append([new_x, new_y, x2, y2, 0.0, 0.0, 0.0])
                # re-execute pick OBJ1
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append([random.uniform(0, 1), random.uniform(0, 1), x2, y2, 0.0, 0.0, 0.0])
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg.append([-1.0, -1.0])
                # Pick okay
                environment.append([0.0, 0.0, x2, y2, 0.0, 0.0, 1.0])
                # Place again
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg.append([temp_x, temp_y])

            # Successful, environment observation is correct now
            environment.append([temp_x, temp_y, x2, y2, 0.0, 0.0, 0.0])
            # Can proceed with output: pick OBJ2
            output.append([0.0, 1.0, 0.0, 0.0])
            out_arg.append([-1.0, -1.0])

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([temp_x, temp_y, random.uniform(0, 1), random.uniform(0, 1), 0.0, 0.0, 0.0])
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

            # Pick successful, generate correct environment observation, set occupied gripper
            environment.append([temp_x, temp_y, 0.0, 0.0, 0.0, 0.0, 1.0])
            # Place Object grasped (OBJ2) in original position of OBJ1
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg.append([x1, y1])

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([temp_x, temp_y, random.uniform(0, 1), random.uniform(0, 1), 0.0, 0.0, 0.0])
                # Re-execute pick OBJ2
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append([temp_x, temp_y, random.uniform(0, 1), random.uniform(0, 1), 0.0, 0.0, 0.0])
                    output.append([0.0, 1.0, 0.0, 0.0])
                    out_arg.append([-1.0, -1.0])

                # Pick Okay
                environment.append([temp_x, temp_y, 0.0, 0.0, 0.0, 0.0, 1.0])
                # Retry Place OBJ2
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg.append([x1, y1])

            # Succeded Place, generate env
            environment.append([temp_x, temp_y, x1, y1, 0.0, 0.0, 0.0])
            # Output: Pick OBJ1
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg.append([-1.0, -1.0])

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([random.uniform(0, 1), random.uniform(0, 1), x1, y1, 0.0, 0.0, 0.0])
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

            # Pick succeded
            environment.append([0.0, 0.0, x1, y1, 0.0, 0.0, 1.0])
            # Place OBJ1 in OBJ2 orinal position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg.append([x2, y2])

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append([random.uniform(0, 1), random.uniform(0, 1), x1, y1, 0.0, 0.0, 0.0])
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg.append([-1.0, -1.0])

                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append([random.uniform(0, 1), random.uniform(0, 1), x1, y1, 0.0, 0.0, 0.0])
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg.append([-1.0, -1.0])

                environment.append([0.0, 0.0, x1, y1, 0.0, 0.0, 1.0])
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg.append([x2, y2])

            # Place successful
            environment.append([x2, y2, x1, y1, 0.0, 0.0, 0.0])
            output.append([0.0, 0.0, 0.0, 1.0])
            out_arg.append([-1.0, -1.0])

            # Manual Padding
            if (len(environment) > self.maxlen):
                print("ERROR SEQ TOO LONG")

            if( self.pad == True):
                while (len(environment) < self.maxlen):
                    environment.append([-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0])
                    output.append([-1.0, -1.0, -1.0, -1.0])
                    out_arg.append([-1.0, -1.0])

            if (i < self.ratio * self.nr_training_examples):
                test_in_list.append(list(environment))
                test_out_list.append(list(output))
                test_arg_list.append(list(out_arg))
            else:
                in_list.append(list(environment))
                out_list.append(list(output))
                arg_list.append(list(out_arg))

        return (np.array(list(in_list)), np.array(list(out_list)), np.array(list(arg_list)), \
                np.array(list(test_in_list)), np.array(list(test_out_list)), np.array(list(test_arg_list)))

    #Generate Discrete Environment (For a single timestep of a single training example)
    def generate_environment(self, x1, y1, x2, y2, gripper):
        vect = []
        if (x1 == -1):
            return [0.0] * 41

        for k in range(0, 41):
            if (k == x1 or k == 10 + y1 or k == 20 + x2 or k == 30 + y2):
                vect.append(1.0)
            else:
                if (k == 40 and gripper == 1.0):
                    vect.append(1.0)
                else:
                    vect.append(0.0)
        return vect

    #Generate Discrete Arguments Output
    def generate_arguments(self, n):
        vect = []
        for k in range(0, 10):
            if (k == n):
                vect.append(1.0)
            else:
                vect.append(0.0)
        return vect

    #Discrete Generation: 3 Output Layers (args separated)
    def generate_discrete(self):
        # Sequence of environment observation and output for the training example
        environment = []
        output = []
        out_arg1 = []
        out_arg2 = []

        in_list = []
        out_list = []
        arg1_list = []
        arg2_list = []

        test_in_list = []
        test_out_list = []
        test_arg1_list = []
        test_arg2_list = []

        # Dataset Generation
        for i in range(self.nr_training_examples):
            # Resets
            environment.clear()
            output.clear()
            out_arg1.clear()
            out_arg2.clear()

            # Note: Position (0,0) should be reserved for when the cube is gripped
            # Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
            x1 = round(random.uniform(1, 9))
            y1 = round(random.uniform(1, 9))

            x2 = round(random.uniform(1, 9))
            y2 = round(random.uniform(1, 9))

            while (x1 == x2):
                x2 = round(random.uniform(1, 9))
            while (y1 == y2):
                y2 = round(random.uniform(1, 9))

            gripper_occupied = 0.0  # Default gripper empty at beginning

            environment.append(self.generate_environment(x1, y1, x2, y2, gripper_occupied))
            # Generate Output: Pick(object1)
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Update Environment: (introduce certain % that pick fails)
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                    x2, y2, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

            # Now Pick is okay, can make the place, set occupied gripper
            environment.append(self.generate_environment(0.0, 0.0, x2, y2, 1.0))
            temp_x = round(random.uniform(1, 9))
            temp_y = round(random.uniform(1, 9))
            # Generate Output: place on random temp position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(temp_x))
            out_arg2.append(self.generate_arguments(temp_y))

            # Update Environment: (introduce certain % that the place fails, wrong position)
            while (random.uniform(0, 1) <= self.error_rate):
                # Place the cube in random position
                new_x = round(random.uniform(1,9))
                new_y = round(random.uniform(1,9))

                environment.append(self.generate_environment(new_x, new_y, x2, y2, 0.0))
                # re-execute pick OBJ1
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment(round(random.uniform(1, 9)), round(random.uniform(1, 9)), x2, y2, 0.0))
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                # Pick okay
                environment.append(self.generate_environment(0.0, 0.0, x2, y2, 1.0))
                # Place again
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(temp_x))
                out_arg2.append(self.generate_arguments(temp_y))

            # Successful, environment observation is correct now
            environment.append(self.generate_environment(temp_x, temp_y, x2, y2, 0.0))
            # Can proceed with output: pick OBJ2
            output.append([0.0, 1.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                        round(random.uniform(1, 9)), 0.0))
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

            # Pick successful, generate correct environment observation, set occupied gripper
            environment.append(self.generate_environment(temp_x, temp_y, 0.0, 0.0, 1.0))
            # Place Object grasped (OBJ2) in original position of OBJ1
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(x1))
            out_arg2.append(self.generate_arguments(y1))

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                        round(random.uniform(1, 9)), 0.0))
                # Re-execute pick OBJ2
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                            round(random.uniform(1, 9)), 0.0))
                    output.append([0.0, 1.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                # Pick Okay
                environment.append(self.generate_environment(temp_x, temp_y, 0.0, 0.0, 1.0))
                # Retry Place OBJ2
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(x1))
                out_arg2.append(self.generate_arguments(y1))

            # Succeded Place, generate env
            environment.append(self.generate_environment(temp_x, temp_y, x1, y1, 0.0))
            # Output: Pick OBJ1
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                        x1, y1, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

            # Pick succeded
            environment.append(self.generate_environment(0.0, 0.0, x1, y1, 1.0))
            # Place OBJ1 in OBJ2 orinal position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(x2))
            out_arg2.append(self.generate_arguments(y2))

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                        x1, y1, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                            x1, y1, 0.0))
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                environment.append(self.generate_environment(0.0, 0.0, x1, y1, 1.0))
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(x2))
                out_arg2.append(self.generate_arguments(y2))

            # Place successful
            environment.append(self.generate_environment(x2, y2, x1, y1, 0.0))
            output.append([0.0, 0.0, 0.0, 1.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Manual Padding
            if (len(environment) > self.maxlen):
                print("ERROR SEQ TOO LONG")

            if (self.pad == True):
                while (len(environment) < self.maxlen):
                    environment.append(self.generate_environment(-1.0, -1.0, -1.0, -1.0, 0.0))
                    output.append([0.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

            if (i < self.ratio * self.nr_training_examples):
                test_in_list.append(list(environment))
                test_out_list.append(list(output))
                test_arg1_list.append(list(out_arg1))
                test_arg2_list.append(list(out_arg2))
            else:
                in_list.append(list(environment))
                out_list.append(list(output))
                arg1_list.append(list(out_arg1))
                arg2_list.append(list(out_arg2))

        return (np.array(list(in_list)), np.array(list(out_list)), np.array(list(arg1_list)), np.array(list(arg2_list)), \
                np.array(list(test_in_list)), np.array(list(test_out_list)), \
                np.array(list(test_arg1_list)), np.array(list(test_arg2_list)))


    #Generate an array with the correct elements discretized
    #Adds the correct X_Temp and Y_Temp positions to the selected values
    def generate_environment_mod(self, x1, y1, x2, y2, gripper, x_temp=0, y_temp=0):
        vect = []
        if (x1 == -1):
            return [0.0] * 61

        for k in range(0, 61):
            if (k == x1 or k == 10 + y1 or k == 20 + x2 or k == 30 + y2 or k == 40+x_temp or k == 50+y_temp):
                vect.append(1.0)
            else:
                if (k == 60 and gripper == 1.0):
                    vect.append(1.0)
                else:
                    vect.append(0.0)
        return vect

    #Generate an array with the correct elements discretized
    #Does not need to specify Temp (x,y), sets them to (1,0,0,0,...,0) (like NULL-values)
    # def generate_environment_mod(self, x1, y1, x2, y2, gripper):
    #     vect = []
    #     if (x1 == -1):
    #         return [0.0] * 61
    #
    #     for k in range(0, 61):
    #         if (k == x1 or k == 10 + y1 or k == 20 + x2 or k == 30 + y2 or k == 40 or k == 50):
    #             vect.append(1.0)
    #         else:
    #             if (k == 60 and gripper == 1.0):
    #                 vect.append(1.0)
    #             else:
    #                 vect.append(0.0)
    #     return vect

    #Discrete Generation: 3 Output layers (2 for Args)
    #No Random Error Introduction through Tempo positions!
    def generate_discrete_mod(self):
        # Sequence of environment observation and output for the training example
        environment = []
        output = []
        out_arg1 = []
        out_arg2 = []

        in_list = []
        out_list = []
        arg1_list = []
        arg2_list = []

        test_in_list = []
        test_out_list = []
        test_arg1_list = []
        test_arg2_list = []

        # Dataset Generation
        for i in range(self.nr_training_examples):
            # Resets
            environment.clear()
            output.clear()
            out_arg1.clear()
            out_arg2.clear()

            # Note: Position (0,0) should be reserved for when the cube is gripped
            # Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
            x1 = round(random.uniform(1, 9))
            y1 = round(random.uniform(1, 9))

            x2 = round(random.uniform(1, 9))
            y2 = round(random.uniform(1, 9))

            temp_x = round(random.uniform(1, 9))
            temp_y = round(random.uniform(1, 9))

            while (x1 == x2):
                x2 = round(random.uniform(1, 9))
            while (y1 == y2):
                y2 = round(random.uniform(1, 9))

            gripper_occupied = 0.0  # Default gripper empty at beginning

            environment.append(self.generate_environment_mod(x1, y1, x2, y2, gripper_occupied, temp_x, temp_y))
            # Generate Output: Pick(object1)
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Update Environment: (introduce certain % that pick fails)
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                    x2, y2, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

            # Now Pick is okay, can make the place, set occupied gripper
            environment.append(self.generate_environment_mod(0.0, 0.0, x2, y2, 1.0))

            # Generate Output: place on random temp position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(temp_x))
            out_arg2.append(self.generate_arguments(temp_y))

            # Update Environment: (introduce certain % that the place fails, wrong position)
            while (random.uniform(0, 1) <= self.error_rate):
                # Place the cube in random position
                new_x = round(random.uniform(1,9))
                new_y = round(random.uniform(1,9))

                environment.append(self.generate_environment_mod(new_x, new_y, x2, y2, 0.0))
                # re-execute pick OBJ1
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), x2, y2, 0.0))
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                # Pick okay
                environment.append(self.generate_environment_mod(0.0, 0.0, x2, y2, 1.0))
                # Place again
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(temp_x))
                out_arg2.append(self.generate_arguments(temp_y))

            # Successful, environment observation is correct now
            environment.append(self.generate_environment_mod(temp_x, temp_y, x2, y2, 0.0))
            # Can proceed with output: pick OBJ2
            output.append([0.0, 1.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                        round(random.uniform(1, 9)), 0.0))
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

            # Pick successful, generate correct environment observation, set occupied gripper
            environment.append(self.generate_environment_mod(temp_x, temp_y, 0.0, 0.0, 1.0))
            # Place Object grasped (OBJ2) in original position of OBJ1
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(x1))
            out_arg2.append(self.generate_arguments(y1))

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                        round(random.uniform(1, 9)), 0.0))
                # Re-execute pick OBJ2
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                            round(random.uniform(1, 9)), 0.0))
                    output.append([0.0, 1.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                # Pick Okay
                environment.append(self.generate_environment_mod(temp_x, temp_y, 0.0, 0.0, 1.0))
                # Retry Place OBJ2
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(x1))
                out_arg2.append(self.generate_arguments(y1))

            # Succeded Place, generate env
            environment.append(self.generate_environment_mod(temp_x, temp_y, x1, y1, 0.0))
            # Output: Pick OBJ1
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                        x1, y1, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

            # Pick succeded
            environment.append(self.generate_environment_mod(0.0, 0.0, x1, y1, 1.0))
            # Place OBJ1 in OBJ2 orinal position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(x2))
            out_arg2.append(self.generate_arguments(y2))

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                        x1, y1, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                            x1, y1, 0.0))
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                environment.append(self.generate_environment_mod(0.0, 0.0, x1, y1, 1.0))
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(x2))
                out_arg2.append(self.generate_arguments(y2))

            # Place successful
            environment.append(self.generate_environment_mod(x2, y2, x1, y1, 0.0))
            output.append([0.0, 0.0, 0.0, 1.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Manual Padding
            if (len(environment) > self.maxlen):
                print("ERROR SEQ TOO LONG")

            if (self.pad == True):
                while (len(environment) < self.maxlen):
                    environment.append(self.generate_environment_mod(-1.0, -1.0, -1.0, -1.0, 0.0))
                    output.append([0.0, 0.0, 0.0, 1.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

            if (i < self.ratio * self.nr_training_examples):
                test_in_list.append(list(environment))
                test_out_list.append(list(output))
                test_arg1_list.append(list(out_arg1))
                test_arg2_list.append(list(out_arg2))
            else:
                in_list.append(list(environment))
                out_list.append(list(output))
                arg1_list.append(list(out_arg1))
                arg2_list.append(list(out_arg2))

        return (np.array(list(in_list)), np.array(list(out_list)), np.array(list(arg1_list)), np.array(list(arg2_list)), \
                np.array(list(test_in_list)), np.array(list(test_out_list)),
                np.array(list(test_arg1_list)), np.array(list(test_arg2_list)))

    # Integrates a single limit case where the first pick always fails
    # but let the task be completed in the provided maxlen
    def generate_discrete_single(self, ratio=0):
            # Sequence of environment observation and output for the training example
            environment = []
            output = []
            out_arg1 = []
            out_arg2 = []

            in_list = []
            out_list = []
            arg1_list = []
            arg2_list = []

            test_in_list = []
            test_out_list = []
            test_arg1_list = []
            test_arg2_list = []

            # Dataset Generation
            for i in range(self.nr_training_examples):
                # Resets
                environment.clear()
                output.clear()
                out_arg1.clear()
                out_arg2.clear()

                # Note: Position (0,0) should be reserved for when the cube is gripped
                # Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
                x1 = round(random.uniform(1, 9))
                y1 = round(random.uniform(1, 9))

                x2 = round(random.uniform(1, 9))
                y2 = round(random.uniform(1, 9))

                temp_x = round(random.uniform(1, 9))
                temp_y = round(random.uniform(1, 9))

                while (x1 == x2):
                    x2 = round(random.uniform(1, 9))
                while (y1 == y2):
                    y2 = round(random.uniform(1, 9))

                gripper_occupied = 0.0  # Default gripper empty at beginning

                environment.append(self.generate_environment_mod(x1, y1, x2, y2, gripper_occupied, temp_x, temp_y))
                # Generate Output: Pick(object1)
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # Update Environment: (introduce certain % that pick fails)
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(
                        self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                      x2, y2, 0.0))
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                # Now Pick is okay, can make the place, set occupied gripper
                environment.append(self.generate_environment_mod(0.0, 0.0, x2, y2, 1.0))

                # Generate Output: place on random temp position
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(temp_x))
                out_arg2.append(self.generate_arguments(temp_y))

                # Update Environment: (introduce certain % that the place fails, wrong position)
                while (random.uniform(0, 1) <= self.error_rate):
                    # Place the cube in random position
                    new_x = round(random.uniform(1, 9))
                    new_y = round(random.uniform(1, 9))

                    environment.append(self.generate_environment_mod(new_x, new_y, x2, y2, 0.0))
                    # re-execute pick OBJ1
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                    # This pick can fail too
                    while (random.uniform(0, 1) <= self.error_rate):
                        environment.append(
                            self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), x2,
                                                          y2, 0.0))
                        output.append([1.0, 0.0, 0.0, 0.0])
                        out_arg1.append(self.generate_arguments(0))
                        out_arg2.append(self.generate_arguments(0))

                    # Pick okay
                    environment.append(self.generate_environment_mod(0.0, 0.0, x2, y2, 1.0))
                    # Place again
                    output.append([0.0, 0.0, 1.0, 0.0])
                    out_arg1.append(self.generate_arguments(temp_x))
                    out_arg2.append(self.generate_arguments(temp_y))

                # Successful, environment observation is correct now
                environment.append(self.generate_environment_mod(temp_x, temp_y, x2, y2, 0.0))
                # Can proceed with output: pick OBJ2
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # Usual error probability
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                                     round(random.uniform(1, 9)), 0.0))
                    output.append([0.0, 1.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                # Pick successful, generate correct environment observation, set occupied gripper
                environment.append(self.generate_environment_mod(temp_x, temp_y, 0.0, 0.0, 1.0))
                # Place Object grasped (OBJ2) in original position of OBJ1
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(x1))
                out_arg2.append(self.generate_arguments(y1))

                # Failure Place
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                                     round(random.uniform(1, 9)), 0.0))
                    # Re-execute pick OBJ2
                    output.append([0.0, 1.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                    # This pick can fail too
                    while (random.uniform(0, 1) <= self.error_rate):
                        environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                                         round(random.uniform(1, 9)), 0.0))
                        output.append([0.0, 1.0, 0.0, 0.0])
                        out_arg1.append(self.generate_arguments(0))
                        out_arg2.append(self.generate_arguments(0))

                    # Pick Okay
                    environment.append(self.generate_environment_mod(temp_x, temp_y, 0.0, 0.0, 1.0))
                    # Retry Place OBJ2
                    output.append([0.0, 0.0, 1.0, 0.0])
                    out_arg1.append(self.generate_arguments(x1))
                    out_arg2.append(self.generate_arguments(y1))

                # Succeded Place, generate env
                environment.append(self.generate_environment_mod(temp_x, temp_y, x1, y1, 0.0))
                # Output: Pick OBJ1
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # Usual error probability
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(
                        self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                      x1, y1, 0.0))
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                # Pick succeded
                environment.append(self.generate_environment_mod(0.0, 0.0, x1, y1, 1.0))
                # Place OBJ1 in OBJ2 orinal position
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(x2))
                out_arg2.append(self.generate_arguments(y2))

                # Failure Place
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(
                        self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                      x1, y1, 0.0))
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                    while (random.uniform(0, 1) <= self.error_rate):
                        environment.append(
                            self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                          x1, y1, 0.0))
                        output.append([1.0, 0.0, 0.0, 0.0])
                        out_arg1.append(self.generate_arguments(0))
                        out_arg2.append(self.generate_arguments(0))

                    environment.append(self.generate_environment_mod(0.0, 0.0, x1, y1, 1.0))
                    output.append([0.0, 0.0, 1.0, 0.0])
                    out_arg1.append(self.generate_arguments(x2))
                    out_arg2.append(self.generate_arguments(y2))

                # Place successful
                environment.append(self.generate_environment_mod(x2, y2, x1, y1, 0.0))
                output.append([0.0, 0.0, 0.0, 1.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # Manual Padding
                if (len(environment) > self.maxlen):
                    print("ERROR SEQ TOO LONG")

                if (self.pad == True):
                    while (len(environment) < self.maxlen):
                        environment.append(self.generate_environment_mod(-1.0, -1.0, -1.0, -1.0, 0.0))
                        output.append([0.0, 0.0, 0.0, 0.0])
                        out_arg1.append(self.generate_arguments(0))
                        out_arg2.append(self.generate_arguments(0))

                if (i < self.ratio * self.nr_training_examples):
                    test_in_list.append(list(environment))
                    test_out_list.append(list(output))
                    test_arg1_list.append(list(out_arg1))
                    test_arg2_list.append(list(out_arg2))
                else:
                    in_list.append(list(environment))
                    out_list.append(list(output))
                    arg1_list.append(list(out_arg1))
                    arg2_list.append(list(out_arg2))

            # Add sequence with single failure repeated on the same operation
            environment.clear()
            output.clear()
            out_arg1.clear()
            out_arg2.clear()

            # Note: Position (0,0) should be reserved for when the cube is gripped
            # Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
            x1 = round(random.uniform(1, 9))
            y1 = round(random.uniform(1, 9))

            x2 = round(random.uniform(1, 9))
            y2 = round(random.uniform(1, 9))

            temp_x = round(random.uniform(1, 9))
            temp_y = round(random.uniform(1, 9))

            while (x1 == x2):
                x2 = round(random.uniform(1, 9))
            while (y1 == y2):
                y2 = round(random.uniform(1, 9))

            gripper_occupied = 0.0  # Default gripper empty at beginning

            environment.append(self.generate_environment_mod(x1, y1, x2, y2, gripper_occupied, temp_x, temp_y))
            # Generate Output: Pick(object1)
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            #Fails up to maxlen - 7 times?
            for i in range(self.maxlen - 7):
                environment.append(
                    self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                          x2, y2, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

            # Now Pick is okay, can make the place, set occupied gripper
            environment.append(self.generate_environment_mod(0.0, 0.0, x2, y2, 1.0))

            # Generate Output: place on random temp position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(temp_x))
            out_arg2.append(self.generate_arguments(temp_y))

            # Successful, environment observation is correct now
            environment.append(self.generate_environment_mod(temp_x, temp_y, x2, y2, 0.0))
            # Can proceed with output: pick OBJ2
            output.append([0.0, 1.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Pick successful, generate correct environment observation, set occupied gripper
            environment.append(self.generate_environment_mod(temp_x, temp_y, 0.0, 0.0, 1.0))
            # Place Object grasped (OBJ2) in original position of OBJ1
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(x1))
            out_arg2.append(self.generate_arguments(y1))

            # Succeded Place, generate env
            environment.append(self.generate_environment_mod(temp_x, temp_y, x1, y1, 0.0))
            # Output: Pick OBJ1
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Pick succeded
            environment.append(self.generate_environment_mod(0.0, 0.0, x1, y1, 1.0))
            # Place OBJ1 in OBJ2 orinal position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(x2))
            out_arg2.append(self.generate_arguments(y2))

            # Place successful
            environment.append(self.generate_environment_mod(x2, y2, x1, y1, 0.0))
            output.append([0.0, 0.0, 0.0, 1.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            #No Padding needed
            if (len(environment) > self.maxlen):
                print("ERROR SEQ TOO LONG")

            #Add it to training set
            if ratio == 0:
                in_list.append(list(environment))
                out_list.append(list(output))
                arg1_list.append(list(out_arg1))
                arg2_list.append(list(out_arg2))
            else:
                for i in range(math.floor(ratio * (1 - self.ratio) * self.nr_training_examples)):
                    in_list.append(list(environment))
                    out_list.append(list(output))
                    arg1_list.append(list(out_arg1))
                    arg2_list.append(list(out_arg2))

            return (
            np.array(list(in_list)), np.array(list(out_list)), np.array(list(arg1_list)), np.array(list(arg2_list)), \
            np.array(list(test_in_list)), np.array(list(test_out_list)),
            np.array(list(test_arg1_list)), np.array(list(test_arg2_list)))

    # Integrates one sample for each (first-level) action failure up to maxlen
    # First pick, first place, second pick etc
    # Nested actions don't fail

    # Should rebalance the actual numbers on training/eval since they are off with this mod
    # for example with 3000 samples: 1804 training + 602 samples
    # so 1798 standard + 6 limit cases
    def generate_discrete_complete(self, ratio=0):
        # Sequence of environment observation and output for the training example
        environment = []
        output = []
        out_arg1 = []
        out_arg2 = []

        in_list = []
        out_list = []
        arg1_list = []
        arg2_list = []

        test_in_list = []
        test_out_list = []
        test_arg1_list = []
        test_arg2_list = []

        # Dataset Generation
        for i in range(self.nr_training_examples):
            # Resets
            environment.clear()
            output.clear()
            out_arg1.clear()
            out_arg2.clear()

            # Note: Position (0,0) should be reserved for when the cube is gripped
            # Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
            x1 = round(random.uniform(1, 9))
            y1 = round(random.uniform(1, 9))

            x2 = round(random.uniform(1, 9))
            y2 = round(random.uniform(1, 9))

            temp_x = round(random.uniform(1, 9))
            temp_y = round(random.uniform(1, 9))

            while (x1 == x2):
                x2 = round(random.uniform(1, 9))
            while (y1 == y2):
                y2 = round(random.uniform(1, 9))

            gripper_occupied = 0.0  # Default gripper empty at beginning

            environment.append(self.generate_environment_mod(x1, y1, x2, y2, gripper_occupied, temp_x, temp_y))
            # Generate Output: Pick(object1)
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Update Environment: (introduce certain % that pick fails)
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                    x2, y2, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

            # Now Pick is okay, can make the place, set occupied gripper
            environment.append(self.generate_environment_mod(0.0, 0.0, x2, y2, 1.0))

            # Generate Output: place on random temp position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(temp_x))
            out_arg2.append(self.generate_arguments(temp_y))

            # Update Environment: (introduce certain % that the place fails, wrong position)
            while (random.uniform(0, 1) <= self.error_rate):
                # Place the cube in random position
                new_x = round(random.uniform(1,9))
                new_y = round(random.uniform(1,9))

                environment.append(self.generate_environment_mod(new_x, new_y, x2, y2, 0.0))
                # re-execute pick OBJ1
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), x2, y2, 0.0))
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                # Pick okay
                environment.append(self.generate_environment_mod(0.0, 0.0, x2, y2, 1.0))
                # Place again
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(temp_x))
                out_arg2.append(self.generate_arguments(temp_y))

            # Successful, environment observation is correct now
            environment.append(self.generate_environment_mod(temp_x, temp_y, x2, y2, 0.0))
            # Can proceed with output: pick OBJ2
            output.append([0.0, 1.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                        round(random.uniform(1, 9)), 0.0))
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

            # Pick successful, generate correct environment observation, set occupied gripper
            environment.append(self.generate_environment_mod(temp_x, temp_y, 0.0, 0.0, 1.0))
            # Place Object grasped (OBJ2) in original position of OBJ1
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(x1))
            out_arg2.append(self.generate_arguments(y1))

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                        round(random.uniform(1, 9)), 0.0))
                # Re-execute pick OBJ2
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # This pick can fail too
                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                            round(random.uniform(1, 9)), 0.0))
                    output.append([0.0, 1.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                # Pick Okay
                environment.append(self.generate_environment_mod(temp_x, temp_y, 0.0, 0.0, 1.0))
                # Retry Place OBJ2
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(x1))
                out_arg2.append(self.generate_arguments(y1))

            # Succeded Place, generate env
            environment.append(self.generate_environment_mod(temp_x, temp_y, x1, y1, 0.0))
            # Output: Pick OBJ1
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Usual error probability
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                        x1, y1, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

            # Pick succeded
            environment.append(self.generate_environment_mod(0.0, 0.0, x1, y1, 1.0))
            # Place OBJ1 in OBJ2 orinal position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(x2))
            out_arg2.append(self.generate_arguments(y2))

            # Failure Place
            while (random.uniform(0, 1) <= self.error_rate):
                environment.append(self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                        x1, y1, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                while (random.uniform(0, 1) <= self.error_rate):
                    environment.append(self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                            x1, y1, 0.0))
                    output.append([1.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

                environment.append(self.generate_environment_mod(0.0, 0.0, x1, y1, 1.0))
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(x2))
                out_arg2.append(self.generate_arguments(y2))

            # Place successful
            environment.append(self.generate_environment_mod(x2, y2, x1, y1, 0.0))
            output.append([0.0, 0.0, 0.0, 1.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Manual Padding
            if (len(environment) > self.maxlen):
                print("ERROR SEQ TOO LONG")

            if (self.pad == True):
                while (len(environment) < self.maxlen):
                    environment.append(self.generate_environment_mod(-1.0, -1.0, -1.0, -1.0, 0.0))
                    output.append([0.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

            if (i < self.ratio * self.nr_training_examples):
                test_in_list.append(list(environment))
                test_out_list.append(list(output))
                test_arg1_list.append(list(out_arg1))
                test_arg2_list.append(list(out_arg2))
            else:
                in_list.append(list(environment))
                out_list.append(list(output))
                arg1_list.append(list(out_arg1))
                arg2_list.append(list(out_arg2))

        #Add sequence with failure on the same operation
        environment = []
        output = []
        out_arg1 = []
        out_arg2 = []

        #Include all the first level failure
        #Each sequence fail everytime (up to maxlen - 7) on the same action

        for example in range(6):
            # Selection of the action
            action = [False, False, False, False, False, False]
            action[example] = True
            counter = 0
            # Resets
            environment.clear()
            output.clear()
            out_arg1.clear()
            out_arg2.clear()

            # Note: Position (0,0) should be reserved for when the cube is gripped
            # Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
            x1 = round(random.uniform(1, 9))
            y1 = round(random.uniform(1, 9))

            x2 = round(random.uniform(1, 9))
            y2 = round(random.uniform(1, 9))

            temp_x = round(random.uniform(1, 9))
            temp_y = round(random.uniform(1, 9))

            while (x1 == x2):
                x2 = round(random.uniform(1, 9))
            while (y1 == y2):
                y2 = round(random.uniform(1, 9))

            gripper_occupied = 0.0  # Default gripper empty at beginning

            environment.append(self.generate_environment_mod(x1, y1, x2, y2, gripper_occupied, temp_x, temp_y))
            # Generate Output: Pick(object1)
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Update Environment: (introduce certain % that pick fails)
            while (action[0] and (counter < self.maxlen - 7)):
                environment.append(
                    self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                  x2, y2, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))
                counter += 1

            # Now Pick is okay, can make the place, set occupied gripper
            environment.append(self.generate_environment_mod(0.0, 0.0, x2, y2, 1.0))

            # Generate Output: place on random temp position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(temp_x))
            out_arg2.append(self.generate_arguments(temp_y))

            # Update Environment place fails
            while (action[1] and counter < math.floor((self.maxlen - 7)/2)):
                # Place the cube in random position
                new_x = round(random.uniform(1, 9))
                new_y = round(random.uniform(1, 9))

                environment.append(self.generate_environment_mod(new_x, new_y, x2, y2, 0.0))
                # re-execute pick OBJ1
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # Pick okay cannot fail anymore in this case
                environment.append(self.generate_environment_mod(0.0, 0.0, x2, y2, 1.0))
                # Place again
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(temp_x))
                out_arg2.append(self.generate_arguments(temp_y))
                counter += 1

            # Successful, environment observation is correct now
            environment.append(self.generate_environment_mod(temp_x, temp_y, x2, y2, 0.0))
            # Can proceed with output: pick OBJ2
            output.append([0.0, 1.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Usual error probability
            while (action[2] and counter < self.maxlen - 7):
                environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                                 round(random.uniform(1, 9)), 0.0))
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))
                counter += 1

            # Pick successful, generate correct environment observation, set occupied gripper
            environment.append(self.generate_environment_mod(temp_x, temp_y, 0.0, 0.0, 1.0))
            # Place Object grasped (OBJ2) in original position of OBJ1
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(x1))
            out_arg2.append(self.generate_arguments(y1))

            # Failure Place
            while (action[3] and counter < math.floor((self.maxlen - 7)/2)):
                environment.append(self.generate_environment_mod(temp_x, temp_y, round(random.uniform(1, 9)), \
                                                                 round(random.uniform(1, 9)), 0.0))
                # Re-execute pick OBJ2
                output.append([0.0, 1.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                # Pick Okay
                environment.append(self.generate_environment_mod(temp_x, temp_y, 0.0, 0.0, 1.0))
                # Retry Place OBJ2
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(x1))
                out_arg2.append(self.generate_arguments(y1))
                counter += 1

            # Succeded Place, generate env
            environment.append(self.generate_environment_mod(temp_x, temp_y, x1, y1, 0.0))
            # Output: Pick OBJ1
            output.append([1.0, 0.0, 0.0, 0.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            while (action[4] and counter < self.maxlen - 7):
                environment.append(
                    self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                  x1, y1, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))
                counter += 1

            # Pick succeded
            environment.append(self.generate_environment_mod(0.0, 0.0, x1, y1, 1.0))
            # Place OBJ1 in OBJ2 orinal position
            output.append([0.0, 0.0, 1.0, 0.0])
            out_arg1.append(self.generate_arguments(x2))
            out_arg2.append(self.generate_arguments(y2))

            # Failure Place
            while (action[5] and counter < math.floor((self.maxlen - 7)/2)):
                environment.append(
                    self.generate_environment_mod(round(random.uniform(1, 9)), round(random.uniform(1, 9)), \
                                                  x1, y1, 0.0))
                output.append([1.0, 0.0, 0.0, 0.0])
                out_arg1.append(self.generate_arguments(0))
                out_arg2.append(self.generate_arguments(0))

                environment.append(self.generate_environment_mod(0.0, 0.0, x1, y1, 1.0))
                output.append([0.0, 0.0, 1.0, 0.0])
                out_arg1.append(self.generate_arguments(x2))
                out_arg2.append(self.generate_arguments(y2))
                counter += 1

            # Place successful
            environment.append(self.generate_environment_mod(x2, y2, x1, y1, 0.0))
            output.append([0.0, 0.0, 0.0, 1.0])
            out_arg1.append(self.generate_arguments(0))
            out_arg2.append(self.generate_arguments(0))

            # Padding -> Needed for fail on the place (makes 2 action again Pick and Place)
            # Sequence length can be odd or even, add as a precaution
            if (self.pad == True):
                while (len(environment) < self.maxlen):
                    environment.append(self.generate_environment_mod(-1.0, -1.0, -1.0, -1.0, 0.0))
                    output.append([0.0, 0.0, 0.0, 0.0])
                    out_arg1.append(self.generate_arguments(0))
                    out_arg2.append(self.generate_arguments(0))

            # Debug
            #for i in range(len(environment)):
            #    print("Timestep: ", i)
            #    print("env: ", environment[i][0:10])
            #    print("env: ", environment[i][10:20])
            #    print("env: ", environment[i][20:30])
            #    print("env: ", environment[i][30:40])
            #    print("env: ", environment[i][40:50])
            #    print("env: ", environment[i][50:])
            #    print("out: ", output[i])
            #    print("arg: ", out_arg1[i])
            #    print("   : ", out_arg2[i])

            # Put the sample inside the training lists
            if ratio == 0:
                in_list.append(list(environment))
                out_list.append(list(output))
                arg1_list.append(list(out_arg1))
                arg2_list.append(list(out_arg2))
            else:
                for i in range(math.floor(ratio / 6 * (1 - self.ratio) * self.nr_training_examples)):
                    in_list.append(list(environment))
                    out_list.append(list(output))
                    arg1_list.append(list(out_arg1))
                    arg2_list.append(list(out_arg2))


        return (np.array(list(in_list)), np.array(list(out_list)), np.array(list(arg1_list)), np.array(list(arg2_list)), \
                np.array(list(test_in_list)), np.array(list(test_out_list)),
                np.array(list(test_arg1_list)), np.array(list(test_arg2_list)))


