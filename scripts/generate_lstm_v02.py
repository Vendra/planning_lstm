# Federico Vendramin 7 Dic 2017
# Generate an artificial dataset and then train a simple LSTM network
# Multiple Output layers. Dense(4) Action and (Dense(2)) for arguments
# Using different losses.

import random
import numpy as np
from keras.models import Model
from keras.layers import Dense, LSTM, Masking, Dropout, Input, TimeDistributed
from keras.optimizers import adam
import matplotlib.pyplot as plt
from swap_dataset_generator import SwapDatasetGenerator

# For the sake of readability
np.set_printoptions(formatter={'float': lambda x: "{0:0.1f}".format(x)})

nr_training_examples = int(input("Insert number of training example: "))
error_rate = float(input("Insert failure probability [0-1]: "))
ratio = 0.2
training_example = []  # Contains all training examples
maxlen = 35

nr_units = 50
nr_epochs = 150
nr_batch = 1

generator = SwapDatasetGenerator(error_rate, nr_training_examples, maxlen, pad=True, ratio=ratio)
# NOTE: Change model shape (shape)
# Standard Generator (None,5)
# (x_train, y_train, arg_train, x_test, y_test, arg_test) = generator.generate_mod()

# Generator without the Temp (x,y) (None, 7)
(x_train, y_train, arg_train, x_test, y_test, arg_test) = generator.generate_mod_ext_temp()

#for k in range(0,x_train.shape[1]):
#    print("X_train: " +str(x_train[0][k]))
#    print("Y_train: " + str(y_train[0][k]))
#    print("A_train: " + str(arg_train[0][k]))

main_input = Input(shape=(None, 7), dtype='float32')
masked_input = Masking(mask_value= -1.0)(main_input)

#lstm = LSTM(units=nr_units, return_sequences=True)(masked_input)
lstm_out = LSTM(units=nr_units, return_sequences=True)(masked_input)
#lstm_dropped = Dropout(0.2)(lstm_out)

out = TimeDistributed(Dense(4, activation='softmax'))(lstm_out)
arg = TimeDistributed(Dense(2))(lstm_out)

model = Model(inputs=[main_input], outputs=[out, arg])
opt = adam(lr=0.001)
model.compile(loss=["categorical_crossentropy","mse"], optimizer=opt, metrics=['accuracy'])
print(model.summary())

history = model.fit(x_train, [y_train, arg_train], validation_split=0.25,
                    epochs=nr_epochs,batch_size=nr_batch, verbose=2)
print(history.history.keys())
plt.figure(1)
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('Total Loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['train', 'val'], loc='upper left')

plt.figure(2)
plt.plot(history.history['time_distributed_1_loss'])
plt.plot(history.history['val_time_distributed_1_loss'])
plt.title('1 Output Layer Loss - Action')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['train', 'val'], loc='upper left')

plt.figure(3)
plt.plot(history.history['time_distributed_2_loss'])
plt.plot(history.history['val_time_distributed_2_loss'])
plt.title('2 Output Layer Loss - Args')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['train', 'val'], loc='upper left')
plt.show()

# Final evaluation of the model
scores = model.evaluate(x_test, [y_test, arg_test], verbose=2)
print(model.metrics_names)
print(scores)

#La seguente non è un accuracy. Loss *100
#print("Accuracy: %.2f%%" % (scores[1] * 100))

save = input("Save model?y/N")
if save == 'y':
    model.save("steps_"+str(nr_training_examples)+"_units_"+str(nr_units)+"_epochs_"+str(nr_epochs)+"_batch_"+str(nr_batch))
    print("Model saved")
    #plot_model(model, to_file="steps_"+str(nr_training_examples)+"_units_"+str(nr_units)+
    #                          "_epochs_"+str(nr_epochs)+"_batch_"+str(nr_batch)+".png")

# -----------CREATE NEW MODEL STATEFUL LOAD WEIGHTS ---------->

eval_input = Input(batch_shape=(1, 1, 7), dtype='float32')
eval_masked_input = Masking(mask_value= -1.0)(eval_input)

eval_lstm_out = LSTM(units=nr_units, return_sequences=True, stateful=True)(eval_masked_input)
#eval_lstm_dropped = Dropout(0.2)(eval_lstm_out)

eval_out = TimeDistributed(Dense(4, activation='softmax'))(eval_lstm_out)
#eval_masked_arg = Masking(mask_value=-1.0)(eval_lstm_dropped)

eval_arg = TimeDistributed(Dense(2))(eval_lstm_out)

eval_model = Model(inputs=[eval_input], outputs=[eval_out, eval_arg])
eval_model.set_weights(model.get_weights())
eval_model.compile('sgd', 'mse')

# ------------------------------------------------------------>

# Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
x1 = random.uniform(0, 1)
y1 = random.uniform(0, 1)

x2 = random.uniform(0, 1)
y2 = random.uniform(0, 1)

#Won't be equal but probably 0.7010234 and 0.07010346 so add a range..
while x1 == x2:
    x2 = random.uniform(0, 1)
while y1 == y2:
    y2 = random.uniform(0, 1)

gripper_occupied = 0.0  # Default gripper empty at beginning
# Give environment to lstm, see prediction
env = []
test_list = []
env.append([x1,y1,x2,y2, 0.5, 0.5, gripper_occupied])

test_list.append(list(env))

x_test = np.array(list(test_list))

print("Environment: X1="+ str(x_test[0][0][0]) + " Y1="+  str(x_test[0][0][1]),
                   "X2="+str(x_test[0][0][2]) + " Y2="+str(x_test[0][0][3]),
                   "TX="+str(x_test[0][0][4]) + " TY="+str(x_test[0][0][5]),
                   "Gripper="+ str(x_test[0][0][6]))


pred = eval_model.predict(x_test, verbose=0)
print("PREDICTION: ", pred)

user_env = []
user_in = []
env_list = []

while pred[0][0][0][3] < 0.5:
    print("Insert Environment: ")

    user_in.clear()
    for i in range(7):
        if i < 4:
            n = input("Env: ")
        elif i < 6:
            n = input("Tmp: ")
        else:
            n = input("Grp: ")
        user_in.append(float(n))

    env.clear()
    env.append([user_in[0], user_in[1], user_in[2], user_in[3], user_in[4], user_in[5], user_in[6]])
    print("Feeding environment to model. " + str(env))
    env_list.clear()
    env_list.append(list(env))
    user_env = np.array(list(env_list))

    np.set_printoptions(formatter={'float': lambda x: "{0:0.1f}".format(x)})
    pred = eval_model.predict(user_env, verbose=1)
    print("Action: "+str(pred[0][0]))
    np.set_printoptions(formatter={'float': lambda x: "{0:0.4f}".format(x)})
    print("Coordinates: "+str(pred[1]))

print("Terminated. Value: " + str(pred[0][0][0][3]))


