# Federico Vendramin 21-12-17
#
# Generate an artificial dataset and then train a simple LSTM network
# Discretized space. Parameters generation is now a classification problem
# Verify that works as intended

import random
import numpy as np
from keras.models import Model
from keras.layers import Dense, LSTM, Masking, Dropout, Input, TimeDistributed

from swap_dataset_generator import SwapDatasetGenerator

error_rate = 0.0
ratio = 0.3
maxlen = 25
nr_units = 30

# Create file log
csv_file = open("discrete_noDrop_adam_results.csv", "a")
columnTitleRow = "Training Samples, Units, Epochs, Batch Size, Loss, Action Loss, Args Loss, Train Loss\n"
csv_file.write(columnTitleRow)

# MODEL
main_input = Input(shape=(None, 61), dtype='float32')
masked_input = Masking(mask_value= 0.0)(main_input)

lstm = LSTM(units=nr_units, return_sequences=True)(masked_input)
#lstm_dropped = Dropout(0.2)(lstm)

out = TimeDistributed(Dense(4,   activation='softmax'))(lstm)
arg1 = TimeDistributed(Dense(10, activation='softmax'))(lstm)
arg2 = TimeDistributed(Dense(10, activation='softmax'))(lstm)

model = Model(inputs=[main_input], outputs=[out, arg1, arg2])
model.compile(loss=["categorical_crossentropy","categorical_crossentropy", "categorical_crossentropy"], \
              optimizer='adam', metrics=['accuracy'])
init_weights = model.get_weights()

#Grid loop
for nr_training_examples in [500, 1500, 3000]:
    training_example = []  # Contains all training examples
    generator = SwapDatasetGenerator(error_rate, nr_training_examples, maxlen, pad=True, ratio=ratio)
    (x_train, y_train, arg1_train, arg2_train, x_test, y_test, arg1_test, arg2_test) = generator.generate_discrete_mod()

    for nr_units in [10, 50, 150]:
        for nr_epochs in [1, 3, 5]:
            for nr_batch in [1, 4, 16, 64]:

                # Reset Weights at each iteration
                model = Model(inputs=[main_input], outputs=[out, arg1, arg2])
                model.compile(loss=["categorical_crossentropy", "categorical_crossentropy", "categorical_crossentropy"], \
                              optimizer='adam', metrics=['accuracy'])
                model.set_weights(init_weights)
                model.reset_states()

                print("Training Examples: " + str(nr_training_examples) + " Units: " + str(nr_units) +
                      " epochs: " + str(nr_epochs) + " batch: " + str(nr_batch))


                hist = model.fit(x_train, [y_train, arg1_train, arg2_train], epochs=nr_epochs, batch_size=nr_batch, verbose=0)

                print(hist.history.get("loss")[nr_epochs - 1])

                # Final evaluation of the model
                scores = model.evaluate(x_test, [y_test, arg1_test, arg2_test], verbose=0)
                print(model.metrics_names)
                print(scores)
                print()

                # Save Results to .csv
                csv_file.write(str(nr_training_examples) + "," + str(nr_units) + "," + str(nr_epochs) + "," +
                               str(nr_batch) + "," + str(scores[0]) + "," + str(scores[1]) + "," +
                               str(scores[2]) + "," + str(hist.history.get("loss")[nr_epochs - 1]) + "\n")

csv_file.close()