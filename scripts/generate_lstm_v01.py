# Federico Vendramin 30 Nov 2017
# Generate an artificial dataset and then train a simple LSTM network
# Single Output TimeDistributed(Dense(6))

import random
from keras.preprocessing import sequence
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, LSTM, Masking, Dropout, Input, TimeDistributed
from swap_dataset_generator import SwapDatasetGenerator


nr_training_examples = int(input("Insert number of training example: "))
error_rate = float(input("Insert failure probability [0-1]: "))
ratio = 0.3
training_example = []  # Contains all training examples
maxlen = 25

generator = SwapDatasetGenerator(error_rate, nr_training_examples, maxlen, pad=True, ratio=ratio)
(x_train, y_train, x_test, y_test) = generator.generate()

# x_train = np.reshape(x_train, np.shape(10))
# x_train = sequence.pad_sequences(x_train, maxlen=maxlen)
# y_train = sequence.pad_sequences(y_train, maxlen=maxlen)
print("X_train shape: ", x_train.shape)
print("Y_train shape: ", y_train.shape)
#print(x_train)

model = Sequential()
nr_units = 100

model.add(Masking(mask_value= -1.0, input_shape=(None, 5)))
model.add(LSTM(units=nr_units, return_sequences=True, activation='relu'))
model.add(Dropout(0.2))
#model.add(LSTM(units=nr_units, return_sequences=True, activation='relu'))
#model.add(Dropout(0.2))
model.add(TimeDistributed(Dense(6, activation='sigmoid')))
model.compile(loss="categorical_crossentropy", optimizer='adam', metrics=['accuracy'])
print(model.summary())
nr_epochs = 5
nr_batch = 8
model.fit(x_train, y_train, epochs=nr_epochs, batch_size=nr_batch, validation_split=0.2)


# Final evaluation of the model
scores = model.evaluate(x_test, y_test, verbose=0)
print("Accuracy: %.2f%%" % (scores[1] * 100))

save = input("Save model?y/N")
if (save == 'y'):
    model.save("steps_"+str(nr_training_examples)+"_units_"+str(nr_units)+"_epochs_"+str(nr_epochs)+"_batch_"+str(nr_batch))
    print("Model saved")

# -----------CREATE NEW MODEL STATEFUL LOAD WEIGHTS ---------->

eval_model = Sequential()
eval_model.add(Masking(mask_value=-1.0, batch_input_shape=(1, 1, 5)))
eval_model.add(LSTM(units=nr_units, return_sequences=True, stateful=True, activation='relu'))
eval_model.add(Dropout(0.2))
#eval_model.add(LSTM(units=nr_units, return_sequences=True, stateful=True, activation='relu'))
#eval_model.add(Dropout(0.2))
eval_model.add(TimeDistributed(Dense(6, activation='sigmoid')))
eval_model.set_weights(model.get_weights())

# ------------------------------------------------------------>



#print("<-- Check dataset - Manual -->")
#print(x_train.shape)
#for i in range(0,10):
#    print("Training example: " + str(i) + " of " + str(nr_training_examples))
#    for j in range(x_train.shape[1]):
#        print("Environment: " + str(x_train[i][j]))
#        print("Output     : " + str(y_train[i][j]))

print("Check model")
# Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
x1 = random.uniform(0, 1)
y1 = random.uniform(0, 1)

x2 = random.uniform(0, 1)
y2 = random.uniform(0, 1)

while (x1 == x2):
    x2 = random.uniform(0, 1)
while (y1 == y2):
    y2 = random.uniform(0, 1)

gripper_occupied = 0.0  # Default gripper empty at beginning
# Give environment to lstm, see prediction
env = []
test_list = []
env.append([x1,y1,x2,y2,gripper_occupied])
test_list.append(list(env))
x_test = np.array(list(test_list))

print("Environment: X1="+ str(x_test[0][0][0]) + " Y1="+  str(x_test[0][0][1]), \
                   "X2="+str(x_test[0][0][2]) + " Y2="+str(x_test[0][0][3]), \
                   "Gripper="+ str(x_test[0][0][4]))

pred = eval_model.predict(x_test)

print("PREDICTION: ")
print(pred)

user_env = []
user_in = []
env_list = []

while(pred[0][0][5] < 0.5):
    print("Insert Environment: ")

    user_in.clear()
    for i in range(5):
        n = input("num :")
        user_in.append(float(n))

    env.clear()
    env.append([user_in[0], user_in[1], user_in[2], user_in[3], user_in[4]])

    env_list.clear()
    env_list.append(list(env))
    user_env = np.array(list(env_list))

    print("Feeding environment to model. ")
    print("Environment: " + str(user_env[0][0]))

    pred = eval_model.predict(user_env)
    print("Out: Pick(O1)="+str(pred[0][0][0])+" Pick(O2)="+str(pred[0][0][1])+" Place()="+str(pred[0][0][2])+\
          " X="+str(pred[0][0][3])+" Y="+str(pred[0][0][4])+" END="+str(pred[0][0][5])+" ")

print("Terminated. Value: " + str(pred[0][0][4]))