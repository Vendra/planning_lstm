import numpy as np
import random
from keras.models import load_model


model = load_model('steps_500_units_1_epochs_3_batch_1')
print(model.summary())

# Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
x1 = random.uniform(0, 1)
y1 = random.uniform(0, 1)

x2 = random.uniform(0, 1)
y2 = random.uniform(0, 1)

while (x1 == x2):
    x2 = random.uniform(0, 1)
while (y1 == y2):
    y2 = random.uniform(0, 1)

gripper_occupied = 0.0  # Default gripper empty at beginning
# Give environment to lstm, see prediction
env = []
test_list = []
env.append([x1,y1,x2,y2,gripper_occupied])

# env.append([ 0.13859833,  0.02548565,  0.78144577,  0.0018128,   0.])
# env.append([ 0.,          0.,          0.78144577,  0.0018128,   1.])
# env.append([ 0.89058047,  0.43697486,  0.78144577,  0.0018128,   0.])
# env.append([ 0.89058047,  0.43697486,  0.,          0.,          1.])
# env.append([ 0.89058047,  0.43697486,  0.13859833,  0.02548565,  0.])
# env.append([ 0.,          0.,          0.13859833,  0.02548565,  1.])
# env.append([ 0.18969974,  0.79050207,  0.13859833,  0.02548565,  0.])
# env.append([ 0.,          0.,          0.13859833,  0.02548565,  1.])
# env.append([ 0.78144577,  0.0018128 ,  0.13859833,  0.02548565,  0.])

test_list.append(list(env))

x_test = np.array(list(test_list))

print("Environment: X1="+ str(x_test[0][0][0]) + " Y1="+  str(x_test[0][0][1]), \
                   "X2="+str(x_test[0][0][2]) + " Y2="+str(x_test[0][0][3]), \
                   "Gripper="+ str(x_test[0][0][4]))


pred = model.predict(x_test, verbose=1)
print("PREDICTION: ", pred)

#for i in range(len(pred[0])):
#    print("Environment: X1=" + str(x_test[0][i][0]) + " Y1=" + str(x_test[0][i][1]), \
#          "X2=" + str(x_test[0][i][2]) + " Y2=" + str(x_test[0][i][3]), \
#          "Gripper=" + str(x_test[0][i][4]))
#    print("Output: Pick(Object1)="+"%.3f" % pred[0][i][0] + " Pick(Object2)="+"%.3f" % pred[0][i][1], \
#                   "Place(Object)="+ str(pred[0][i][2]) + " X="+ str(pred[0][i][3]), \
#                   "Y="+"%.3f" % pred[0][i][3])
#    print("")

user_env = []
user_in = []
env_list = []

#print("Shape: ", pred[0].shape)
while(pred[0][0][0][3] < 0.5):
    print("Insert Environment: ")

    user_in.clear()
    for i in range(5):
        n = input("Env: ")
        user_in.append(float(n))

    env.clear()
    env.append([user_in[0], user_in[1], user_in[2], user_in[3], user_in[4]])
    print("Feeding environment to model. " + str(env))
    env_list.clear()
    env_list.append(list(env))
    user_env = np.array(list(env_list))
    pred = model.predict(user_env, verbose=1)
    print("Action: "+str(pred[0][0]))
    print("Coordinates: "+str(pred[1]))

    #print("Output: Pick(Object1)=" + "%.3f" % pred[0][0][0] + " Pick(Object2)=" + "%.3f" % pred[0][0][1], \
    #      "Place(Object)=" + "%.3f" % pred[0][0][2] + " X=" + str(pred[0][0][3]), \
    #      "Y=" +  str(pred[0][0][3]))


print("Terminated. Value: " + str(pred[0][0][4]))
