import numpy as np
import random
from keras.models import load_model


model = load_model(      'steps_2000_units_30_epochs_3_batch_8')
#reset_model = load_model('steps_2000_units_30_epochs_3_batch_8')

# Generate Environment: Object position 2D (X1,Y1) =! (X2,Y2) € [0,1]
x1 = random.uniform(0, 1)
y1 = random.uniform(0, 1)

x2 = random.uniform(0, 1)
y2 = random.uniform(0, 1)

while (x1 == x2):
    x2 = random.uniform(0, 1)
while (y1 == y2):
    y2 = random.uniform(0, 1)

gripper_occupied = 0.0  # Default gripper empty at beginning
# Give environment to lstm, see prediction
env = []
test_list = []
env.append([x1,y1,x2,y2,gripper_occupied])

# env.append([ 0.13859833,  0.02548565,  0.78144577,  0.0018128,   0.])
# env.append([ 0.,          0.,          0.78144577,  0.0018128,   1.])
# env.append([ 0.89058047,  0.43697486,  0.78144577,  0.0018128,   0.])
# env.append([ 0.89058047,  0.43697486,  0.,          0.,          1.])
# env.append([ 0.89058047,  0.43697486,  0.13859833,  0.02548565,  0.])
# env.append([ 0.,          0.,          0.13859833,  0.02548565,  1.])
# env.append([ 0.18969974,  0.79050207,  0.13859833,  0.02548565,  0.])
# env.append([ 0.,          0.,          0.13859833,  0.02548565,  1.])
# env.append([ 0.78144577,  0.0018128 ,  0.13859833,  0.02548565,  0.])

test_list.append(list(env))

x_test = np.array(list(test_list))

print("Environment: X1="+ str(x_test[0][0][0]) + " Y1="+  str(x_test[0][0][1]), \
                   "X2="+str(x_test[0][0][2]) + " Y2="+str(x_test[0][0][3]), \
                   "Gripper="+ str(x_test[0][0][4]))


pred = model.predict(x_test)
#reset_pred = reset_model.predict(x_test)

print("PREDICTION: ")
print(pred)
print("RESET prediction: ")
print(reset_pred)

user_env = []
user_in = []
env_list = []
env_list.append(list(env))

while(pred[pred.shape[0]-1][0][4] < 0.5):
    print("Insert Environment: ")

    user_in.clear()
    for i in range(5):
        n = input("num :")
        user_in.append(float(n))

    env.clear()
    env.append([user_in[0], user_in[1], user_in[2], user_in[3], user_in[4]])

    #env_list.clear()
    env_list.append(list(env))
    user_env = np.array(list(env_list))

    print("Feeding environment to model. ")
    for k in range(0, user_env.shape[0]):
        print("Environment: " + str(user_env[k][0]))

    pred = model.predict(user_env)

    print("Reset secondary model")
    #reset_model.reset_states()
    #reset_pred = reset_model.predict(user_env)

    for k in range(0, pred.shape[0]):
        print("Out: "+str(pred[k][0]))
        #print("Res: "+str(reset_pred[k][0]))

print("Terminated. Value: " + str(pred[0][0][4]))
