# Federico Vendramin 7 Dic 2017
#
# Grid Test for hyperparameters
#
# Generate an artificial dataset and then train a simple LSTM network
# Continuous Arguments Output layers. Dense(4) Action and (Dense(2)) for arguments

from keras.models import Model
from keras.layers import Dense, LSTM, Masking, Input, TimeDistributed
from swap_dataset_generator import SwapDatasetGenerator

# For the sake of readability
np.set_printoptions(formatter={'float': lambda x: "{0:0.1f}".format(x)})

error_rate = 0.0
ratio = 0.3
maxlen = 25
nr_units = 500  # [10, 25, 50, 100, 150]

# Create file log
csv_file = open("continuous_noDrop_adam_results.csv", "a")
columnTitleRow = "Training Samples, Units, Epochs, Batch Size, Loss, Action Loss, Args Loss, Train Loss\n"
csv_file.write(columnTitleRow)

# MODEL
main_input = Input(shape=(None, 5), dtype='float32')
masked_input = Masking(mask_value=-1.0)(main_input)
lstm = LSTM(units=nr_units, return_sequences=True)(masked_input)
# lstm_dropped = Dropout(0.2)(lstm)
out = TimeDistributed(Dense(4, activation='softmax'))(lstm)
# arg = TimeDistributed(Dense(2))(lstm_dropped)
arg = TimeDistributed(Dense(2))(lstm)

model = Model(inputs=[main_input], outputs=[out, arg])
model.compile(loss=["categorical_crossentropy", "mse"], optimizer='adam')
init_weights = model.get_weights()
# Grid loop
for nr_training_examples in [500, 1500, 3000, 5000]:
    training_example = []  # Contains all training examples
    generator = SwapDatasetGenerator(error_rate, nr_training_examples, maxlen, pad=True, ratio=ratio)
    (x_train, y_train, arg_train, x_test, y_test, arg_test) = generator.generate_mod()  # MODEL

    for nr_epochs in range(1, 11):
        for nr_batch in [1, 4, 16, 64]:
            # Reset Weights at each iteration
            model = Model(inputs=[main_input], outputs=[out, arg])
            model.compile(loss=["categorical_crossentropy", "mse"], optimizer='adam')
            model.set_weights(init_weights)
            model.reset_states()
            print("Training Examples: " + str(nr_training_examples) + " Units: " + str(nr_units) +
                  " epochs: " + str(nr_epochs) + " batch: " + str(nr_batch))

            hist = model.fit(x_train, [y_train, arg_train], epochs=nr_epochs, batch_size=nr_batch, verbose=0)
            # print(hist.history)
            print(hist.history.get("loss")[nr_epochs - 1])
            print(hist.history)
            # Final evaluation of the model
            scores = model.evaluate(x_test, [y_test, arg_test], verbose=0)  # ver=0 -> Silent
            print(model.metrics_names)
            print(scores)
            print()

            # Save Results to .csv
            csv_file.write(str(nr_training_examples) + "," + str(nr_units) + "," + str(nr_epochs) + "," +
                           str(nr_batch) + "," + str(scores[0]) + "," + str(scores[1]) + "," +
                           str(scores[2]) + "," + str(hist.history.get("loss")[nr_epochs - 1]) + "\n")

csv_file.close()
